﻿var app = function() {
    var AppViewModel = function () {
        var that = this;
        this.inputString = ko.observable();
        this.selectedOperation = ko.observable();

        this.sanitizedInput = ko.computed(function() {
            return '"' + that.inputString() + '"';
        });

        this.outputtedValue = ko.observable();

        this.operationUrl = ko.computed(function() {
            return '/api/security/' + that.selectedOperation();
        });

        this.performOperation = function () {

            var request = $.ajax({
                url: that.operationUrl(),
                type: 'POST',
                contentType: 'application/json',
                data: that.sanitizedInput()
            });

            request.success(function(e) {
                that.outputtedValue(e);
            }).fail(function() {
                that.outputtedValue('An error occurred while performing the operation.');
            });
        }
    };

    ko.applyBindings(new AppViewModel());
}();