﻿using System.Web;
using System.Web.Mvc;

namespace NorthshorePartners.SampleProject
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
