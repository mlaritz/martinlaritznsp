﻿using System;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Security;

namespace NorthshorePartners.SampleProject.Controllers
{

    [RoutePrefix("api/security")]
    public class SecurityController : ApiController
    {
        [Route("encrypt")]
        public string PostEncrypt([FromBody]string value)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                return null;
            }

            byte[] stream = Encoding.UTF8.GetBytes(value);
            byte[] encodedValue = MachineKey.Protect(stream);
            return HttpServerUtility.UrlTokenEncode(encodedValue);
        }

        [Route("decrypt")]
        public string PostDecrypt([FromBody]string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return null;
            }
                
            byte[] stream = HttpServerUtility.UrlTokenDecode(value);

            if (stream == null)
            {
                throw new ApplicationException("Input could not be decoded.");
            }

            byte[] decodedValue = MachineKey.Unprotect(stream);
            return decodedValue == null ? null : Encoding.UTF8.GetString(decodedValue);
        }
    }
}
