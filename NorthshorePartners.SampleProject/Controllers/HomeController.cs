﻿using System.Web.Mvc;

namespace NorthshorePartners.SampleProject.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }
    }
}
